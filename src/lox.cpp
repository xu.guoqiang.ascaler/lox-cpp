#include <list>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>

#include "scanner.h"

class Scanner;
class Token;

class Lox {
    static void Interpret(std::vector<std::string> args) {
        if (args.size() > 1) {
          std::cout << "Usage: jlox [script]";
          return;
        } else if (1 == args.size()) {
          RunFile(args[0]);
        } else {
          RunPrompt();
        }
    }

    static void RunPrompt() {
        for (;;) {
            std::cout << "> ";
            std::string line;
            std::getline(std::cin, line);

            if (line.empty()) {
                break;
            }
            Run(line);
            has_error_ = false;
        }
    }

    static void RunFile(std::string path) {
        std::ifstream in_file(path);
        std::string in_str (std::istreambuf_iterator<char>(in_file), (std::istreambuf_iterator<char>()));
        Run(in_str);
        if (has_error_) {
            return;
        }
    }

    static void Run(std::string source) {
        Scanner scanner(source);
        std::list<Token> tokens = scanner.ScanTokens();

        // For now, just print the tokens.
        for (Token token : tokens) {
            std::cout << token.ToString() << std::endl;
        }
    }

    static bool has_error_;
};

bool Lox::has_error_ = false;