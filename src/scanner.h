#pragma once

#include <any>
#include <map>
#include <list>
#include <string>
#include <sstream>

enum class TokenType {

  // Single-character tokens.
  LEFT_PAREN = 0,
  RIGHT_PAREN = 1,
  LEFT_BRACE = 2,
  RIGHT_BRACE = 3,
  COMMA = 4, 
  DOT = 5,
  MINUS = 6,
  PLUS = 7,
  SEMICOLON = 8,
  SLASH = 9,
  STAR = 10,

  // One or two character tokens.
  BANG = 11,
  BANG_EQUAL = 12,
  EQUAL = 13,
  EQUAL_EQUAL = 14,
  GREATER = 15, 
  GREATER_EQUAL = 16,
  LESS = 17,
  LESS_EQUAL = 18,

  // Literals.
  IDENTIFIER = 19,
  STRING = 20,
  NUMBER = 21,

  // Keywords.
  AND = 22,
  CLASS = 23,
  ELSE = 24,
  FALSE = 25,
  FUN = 26,
  FOR= 27,
  IF = 28,
  NIL = 29,
  OR = 30,
  PRINT = 31,
  RETURN = 32,
  SUPER = 33,
  THIS = 34,
  TRUE = 35,
  VAR = 36,
  WHILE = 37,

  END = 38,
};

std::map<TokenType, std::string> static_token_map {

  // Single-character tokens.
  { TokenType::LEFT_PAREN, "LEFT_PAREN" },
  { TokenType::RIGHT_PAREN, "RIGHT_PAREN" },
  { TokenType::LEFT_BRACE, "LEFT_BRACE" },
  { TokenType::RIGHT_BRACE, "RIGHT_BRACE" },
  { TokenType::COMMA, "COMMA" },
  { TokenType::DOT, "DOT" },
  { TokenType::MINUS, "MINUS" },
  { TokenType::PLUS, "PLUS" },
  { TokenType::SEMICOLON, "SEMICOLON" },
  { TokenType::SLASH, "SLASH" },
  { TokenType::STAR, "STAR" },

  // One or two character tokens.
  { TokenType::BANG, "BANG" },
  { TokenType::BANG_EQUAL, "BANG_EQUAL" },
  { TokenType::EQUAL, "EQUAL" },
  { TokenType::EQUAL_EQUAL, "EQUAL_EQUAL" },
  { TokenType::GREATER, "GREATER" }, 
  { TokenType::GREATER_EQUAL, "GREATER_EQUAL" },
  { TokenType::LESS, "LESS" },
  { TokenType::LESS_EQUAL, "LESS_EQUAL" },

  // Literals.
  { TokenType::IDENTIFIER, "IDENTIFIER" },
  { TokenType::STRING, "STRING" },
  { TokenType::NUMBER, "NUMBER" },

  // Keywords.
  { TokenType::AND, "AND" },
  { TokenType::CLASS, "CLASS" },
  { TokenType::ELSE, "ELSE" },
  { TokenType::FALSE, "FALSE" },
  { TokenType::FUN, "FUN" },
  { TokenType::FOR, "FOR" },
  { TokenType::IF, "IF" },
  { TokenType::NIL, "NIL" },
  { TokenType::OR, "OR" },
  { TokenType::PRINT, "POINT" },
  { TokenType::RETURN, "RETURN" },
  { TokenType::SUPER, "SUPER" },
  { TokenType::THIS, "THIS" },
  { TokenType::TRUE, "TRUE" },
  { TokenType::VAR, "VAR" },
  { TokenType::WHILE, "WHILE" },

  { TokenType::END, "END" },
};

std::ostream& operator<<(std::ostream& os, const TokenType& token) {
    os << static_token_map[token];
    return os;
}

class Value {
private:
    std::any value_;
};

std::ostream& operator<<(std::ostream& os, const Value& value) {
    if (typeid(std::string) == value.type()) {
        os << std::any_cast<std::string>(value);
    }
    else if (typeid(float) == value.type()) {
        os << std::any_cast<float>(value);
    }
    else {
        std::stringstream dbg_info;
        dbg_info << "[" __FILE__ << "|" << __LINE__ << "]"
                 << "unknown value type";
        throw std::logic_error(dbg_info.str());
    }
    return os;
}

class Token {

public:
    Token(TokenType type, std::string lexeme, Value literal, int line) {
        type_ = type;
        lexeme_ = lexeme;
        literal_ = literal;
        line_ = line;
    }

    std::string ToString() {
        std::stringstream ss; 
        ss << type_ << " " << lexeme_;
        return ss.str(); 
    }

private:
    TokenType type_;
    std::string lexeme_;
    Value literal_;
    int line_; 
};

class Scanner {
public:
    Scanner(std::string source) {
        source_ = source;
    }

    std::list<Token> ScanTokens() {
        while (!IsAtEnd()) {
            // We are at the beginning of the next lexeme.
            start_ = current_;
            ScanToken();
        }

        tokens_.push_back(Token(TokenType::END, "", Value(), line_));
        return tokens_;
    }

    bool IsAtEnd() {
        return current_ >= source_.size();
    }

    void ScanToken() {
        char c = Advance();
        switch (c) {
          case '(': AddToken(TokenType::LEFT_PAREN); break;
          case ')': AddToken(TokenType::RIGHT_PAREN); break;
          case '{': AddToken(TokenType::LEFT_BRACE); break;
          case '}': AddToken(TokenType::RIGHT_BRACE); break;
          case ',': AddToken(TokenType::COMMA); break;
          case '.': AddToken(TokenType::DOT); break;
          case '-': AddToken(TokenType::MINUS); break;
          case '+': AddToken(TokenType::PLUS); break;
          case ';': AddToken(TokenType::SEMICOLON); break;
          case '*': AddToken(TokenType::STAR); break; 
          case '!': AddToken(Match('=')? TokenType::BANG_EQUAL : TokenType::BANG); break;
          case '=': AddToken(Match('=')? TokenType::EQUAL_EQUAL : TokenType::EQUAL); break;
          case '<': AddToken(Match('=')? TokenType::LESS_EQUAL : TokenType::LESS); break;
          case '>': AddToken(Match('=') ? TokenType::GREATER_EQUAL : TokenType::GREATER); break;
          default:
            std::stringstream ss;
            ss << "[" << __FILE__ << "|" << __LINE__ << "]"
               << "Unexpected character.";
            std::logic_error(ss.str());
            break;
        }
    }

    bool Match(char expected) {
        if (IsAtEnd()) {
            return false;
        }

        if (source_[current_] != expected) {
            return false;
        }

        current_++;
        return true;
    }

    char Advance() {
        return source_[current_++];
    }

    void AddToken(TokenType type) {
        AddToken(type, Value());
    }

    void AddToken(TokenType type, Value value) {
        std::string text = source_.substr(start_, current_);
        tokens_.push_back(Token(type, text, value, line_));
    }
private:
    std::string source_;
    std::list<Token> tokens_;

    int32_t start_ = 0;
    int32_t current_ = 0;
    int32_t line_ = 1;

};